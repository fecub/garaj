# Garaj application 
Repository für einen smarten Garagentor 


### Server 
läuft auf dem Pi 
* steuerungszentrum 
    * die Relay wird ein und ausgeschaltet
    * entfernungssensoren werden beobachten
* automatische ein und ausschalten nach X sekunden 
* API für anfragen ob tor auf oder zu geschlossen ist 
    * von entfernten schließen können

### Client 
läuft auf dem Smartphone
* kommandos senden
    * Garagentor öffnen 
    * Garagentor schließen
