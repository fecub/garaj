import RPi.GPIO as GPIO
import time

# GPIO.setwarnings(False)
# GPIO.setmode(GPIO.BCM)

relay=18

def sinyalyolla():
    GPIO.setup(relay,GPIO.OUT)
    GPIO.output(relay,GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(relay,GPIO.LOW)
    GPIO.cleanup()

def kapiyiac():
    GPIO.setup(relay,GPIO.OUT)

    #durdurur
    GPIO.output(relay,GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(relay,GPIO.LOW)

    time.sleep(2)
    # acar
    GPIO.output(relay,GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(relay,GPIO.LOW)
    
    # clean
    GPIO.cleanup()
