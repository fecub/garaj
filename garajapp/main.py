import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

import time
import garajsinyal as gsinyal
import garajmesafe as gmesafe
import time


kapikapanirken = True

def kontrol():
    global kapikapanirken
    # Kontrol mekanizma - Kapigi acildigi vakit kontrol
    while kapikapanirken:
        kapimesafesi=gmesafe.mesafe(GPIO)
        print ("mesafeiz budur : " + str(kapimesafesi))
        if (kapimesafesi < 76):
            gsinyal.kapiyiac()
            kapikapanirken = False
        time.sleep(1)

def kapiac():
    pass

def kapikapat():
    gsinyal.sinyalyolla()
    kontrol()


if __name__ == '__main__':
    kapikapat()